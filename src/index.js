import React from "react"
import { render } from "react-dom"

import { Provider } from "react-redux"
import { applyMiddleware, compose, createStore } from "redux"
import createSagaMiddleware from "redux-saga"

import rootReducer from "./reducers/root"
import rootSaga from "./sagas/root"
import AnotherApp from "./AnotherApp"

const sagaMiddleware = createSagaMiddleware()
const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(sagaMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
)
sagaMiddleware.run(rootSaga)

render(
  <Provider store={store}>
    <AnotherApp />
  </Provider>,
  document.getElementById("root")
)
