import React, { useState } from "react"
import { useRequest } from "../hooks/request"
import { REQUEST_POSTS } from "../constants/request"

const Form = () => {
  const [, getPosts] = useRequest(REQUEST_POSTS)
  const [userId, setUserId] = useState(null)

  return (
    <div>
      <div>
        <label>
          userId
          <br />
          <input
            type="number"
            onChange={({ target: { value } }) => setUserId(value)}
          />
        </label>
      </div>
      <button onClick={() => getPosts({ userId })}>Submit</button>
    </div>
  )
}

export default Form
