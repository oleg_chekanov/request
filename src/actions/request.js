export const REQUEST = "REQUEST"
export const REQUEST_SUCCESS = "REQUEST_SUCCESS"

export const request = ({ name, params }) => ({
  type: REQUEST,
  name,
  params
})

export const requestSuccess = ({ result, name }) => ({
  type: REQUEST_SUCCESS,
  name,
  result
})
