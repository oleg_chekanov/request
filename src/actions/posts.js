export const GET_POSTS = "GET_POSTS"
export const GET_POSTS_SUCCESS = "GET_POSTS_SUCCESS"

export const getPosts = ({ userId }) => ({
  type: GET_POSTS,
  userId
})

export const getPostsSuccess = ({ posts }) => ({
  type: GET_POSTS_SUCCESS,
  posts
})
