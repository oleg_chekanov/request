import requestConfig from "../requestConfig"

export const createSelector = name => {
  const reducer = requestConfig[name].reducer

  return state => state[reducer]
}
