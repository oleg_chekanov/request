import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"

import { useDispatch, useSelector } from "react-redux"
import { getPosts } from "./actions/posts"

const App = () => {
  const posts = useSelector(({ posts }) => posts)

  const dispatch = useDispatch()
  const [userId, setUserId] = useState(null)

  return (
    <>
      <div>
        <div>
          <label>
            userId
            <br />
            <input
              type="text"
              onChange={({ target: { value } }) => setUserId(value)}
            />
          </label>
        </div>
        <button onClick={() => dispatch(getPosts({ userId }))}>Submit</button>
      </div>
      <hr />
      {posts.length ? (
        posts.map(post => <div key={post.id}>{JSON.stringify(post)}</div>)
      ) : (
        <div>нет постов</div>
      )}
    </>
  )
}

export default App
