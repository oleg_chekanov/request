import { REQUEST_POSTS } from "./constants/request"

const requestConfig = {
  [REQUEST_POSTS]: {
    reducer: "posts",
    url: "https://jsonplaceholder.typicode.com/posts"
  }
}

export default requestConfig
