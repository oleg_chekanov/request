import { useDispatch, useSelector } from "react-redux"

import { request } from "../actions/request"
import { createSelector } from "../selectors/request"

export const useRequest = name => {
  const dispatch = useDispatch()

  const call = params => {
    dispatch(request({ name, params }))
  }

  const selector = createSelector(name)
  const result = useSelector(selector)

  return [result, call]
}
