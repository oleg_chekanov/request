import { call, put, takeLatest, all } from "redux-saga/effects"

import { GET_POSTS, getPostsSuccess } from "../actions/posts"
import request from "./request"

function* rootSaga() {
  yield takeLatest(GET_POSTS, function*({ userId }) {
    const response = yield call(
      fetch,
      `https://jsonplaceholder.typicode.com/posts?userId=${userId}`
    )

    const posts = yield call([response, "json"])

    yield put(getPostsSuccess({ posts }))
  })

  yield all([request()])
}

export default rootSaga
