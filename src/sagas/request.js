import { all, call, put, takeLatest } from "redux-saga/effects"

import { REQUEST, requestSuccess } from "../actions/request"
import requestConfig from "../requestConfig"

function* watchRequest() {
  yield takeLatest(REQUEST, function*({ name, params }) {
    const urlParams = Object.entries(params)
      .map(entry => entry.join("="))
      .join("&")

    const response = yield call(
      fetch,
      `${requestConfig[name].url}?${urlParams}`
    )

    const result = yield call([response, "json"])

    yield put(requestSuccess({ result, name }))
  })
}

export default function*() {
  yield all([watchRequest()])
}
