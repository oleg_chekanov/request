import { GET_POSTS_SUCCESS } from "../actions/posts"
import { REQUEST_SUCCESS } from "../actions/request"

const postsReducer = (state = [], action) => {
  switch (action.type) {
    case GET_POSTS_SUCCESS: {
      return action.posts
    }

    case REQUEST_SUCCESS: {
      return ["lol"]
    }

    default: {
      return state
    }
  }
}

export default postsReducer
