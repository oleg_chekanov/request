import { combineReducers } from "redux"
import reduceReducers from "reduce-reducers"

import postsReducer from "./posts"
import requestReducer from "./request"

const rootReducer = combineReducers({
  posts: postsReducer,
  test: (state = "test") => state
})

export default reduceReducers(rootReducer, requestReducer)
