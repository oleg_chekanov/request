import { REQUEST_SUCCESS } from "../actions/request"
import requestConfig from "../requestConfig"

const requestReducer = (state, action) => {
  switch (action.type) {
    case REQUEST_SUCCESS: {
      const { name, result } = action

      const reducer = requestConfig[name].reducer
      return { ...state, [reducer]: result }
    }
    default: {
      return state
    }
  }
}

export default requestReducer
