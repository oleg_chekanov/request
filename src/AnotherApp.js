import React from "react"

import { useRequest } from "./hooks/request"
import { REQUEST_POSTS } from "./constants/request"
import Form from "./components/From"

const AnotherApp = () => {
  const [posts] = useRequest(REQUEST_POSTS)

  return (
    <>
      <Form />
      <hr />
      {posts.length ? (
        posts.map(post => <div key={post.id}>{JSON.stringify(post)}</div>)
      ) : (
        <div>нет постов</div>
      )}
    </>
  )
}

export default AnotherApp
